# Build script examples

+ [Test and deploy Ruby applications to Heroku](test-and-deploy-ruby-application-to-heroku.md)
+ [Test and deploy Python applications to Heroku](test-and-deploy-python-application-to-heroku.md)
+ [Test Clojure applications](test-clojure-application.md)
